/*
 * Copyright (C) 2019 Justin Luoma <justin@justinluoma.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package cfg

import (
	"os"

	"github.com/spf13/cobra"
	"gitlab.com/justin-luoma/shorter/shorter/cmd/internal"
	"gitlab.com/justin-luoma/shorter/shorter/cmd/internal/config"
)

var GenerateCmd = &cobra.Command{
	Use:   "generate",
	Short: "Generate an example configuration file",
	Run:   generateConfigFile,
}

func InitGenerate() {
	GenerateCmd.Flags().String("format", "yaml",
		"output format (default: yaml), valid options are: yaml, json, toml")

	GenerateCmd.Flags().StringP("file", "f", "config",
		"file to write configuration to (default: ./config.{format}")

	GenerateCmd.Flags().Bool("force", false, "force overwriting of existing config file")
}

func generateConfigFile(cmd *cobra.Command, args []string) {
	configFile, err := cmd.Flags().GetString("file")
	if err != nil {
		internal.PrintError(err)
	}
	format, err := cmd.Flags().GetString("format")
	if err != nil {
		internal.PrintError(err)
	}
	overwrite, err := cmd.Flags().GetBool("force")
	if err != nil {
		internal.PrintError(err)
	}
	file := configFile + "." + format
	if _, err := os.Stat(file); !os.IsNotExist(err) {
		if !overwrite {
			internal.PrintError("file:", file, "already exists, you must --force to overwrite existing file")
			os.Exit(1)
		}
	}
	cfg := config.GetDefaultConfig()
	err = cfg.WriteConfigFile(format, file)
	if err != nil {
		internal.PrintError(err)
		os.Exit(2)
	}
	println("config file written to:", file)
	os.Exit(0)
}
