/*
 * Copyright (C) 2019 Justin Luoma <justin@justinluoma.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package cfg

import (
	"encoding/json"
	"fmt"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

var PrintCmd = &cobra.Command{
	Use:   "print",
	Short: "Print the current configuration",
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Printf("using config file: %s\n", viper.ConfigFileUsed())
		b, err := json.MarshalIndent(viper.AllSettings(), "", " ")
		if err == nil {
			println(string(b))
		}
	},
}
