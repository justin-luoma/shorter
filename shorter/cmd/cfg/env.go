/*
 * Copyright (C) 2019 Justin Luoma <justin@justinluoma.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package cfg

import (
	"github.com/spf13/cobra"
	"gitlab.com/justin-luoma/shorter/shorter/cmd/internal/config"
)

var EnvCmd = &cobra.Command{
	Use:   "env",
	Short: "Print current values of environment variables",
	Run:   printHelp,
}

func printHelp(cmd *cobra.Command, args []string) {
	config.PrintENVHelp()
}
