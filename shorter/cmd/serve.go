/*
 * Copyright (C) 2019 Justin Luoma <justin@justinluoma.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package cmd

import (
	"context"
	"fmt"
	"os"
	"os/signal"
	"time"

	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"gitlab.com/justin-luoma/shorter"
	"gitlab.com/justin-luoma/shorter/repos/postgres"
	"gitlab.com/justin-luoma/shorter/shorter/cmd/internal"
	"gitlab.com/justin-luoma/shorter/shorter/cmd/internal/config"
)

var disableHTTP bool
var disableDNS bool

var serveCmd = &cobra.Command{
	Use:   "serve",
	Short: "Start shorten server",
	Run: func(cmd *cobra.Command, args []string) {
		if cfgError {
			return
		}
		if disableDNS && disableHTTP {
			internal.PrintError("cannot disable both HTTP and DNS servers")
			os.Exit(5)
		}
		cfg := config.ParseAndMerge(cmd)

		startShortenServer(cfg)
	},
}

func initServe() {
	rootCmd.AddCommand(serveCmd)

	serveCmd.PersistentFlags().StringP("loggingLevel", "l", "",
		"set log level, valid options: trace, debug, info, warn, error, fatal, panic; will override config file settings")

	serveCmd.PersistentFlags().String("tld", "",
		"set tld; will override config file settings")

	serveCmd.PersistentFlags().Bool("disableServerLogging", false,
		"disables http and dns server logging; will override config file settings")

	serveCmd.PersistentFlags().String("databaseType", "",
		"sets database type, valid options: postgres; will override config file settings")

	serveCmd.PersistentFlags().String("databaseHost", "",
		"sets database host; will override config file settings")

	serveCmd.PersistentFlags().String("databaseName", "",
		"sets database name; will override config file settings")

	serveCmd.PersistentFlags().String("databaseUser", "",
		"sets database user; will override config file settings")

	serveCmd.PersistentFlags().String("databasePassword", "",
		"sets the password for database user; will override config file settings")

	serveCmd.PersistentFlags().String("databaseSSLMode", "",
		"sets database sslmode, valid options: disable, require, verify-ca, verify-full; will override config file settings")

	serveCmd.PersistentFlags().String("httpAddress", "",
		"sets http listen address; will override config file settings")

	serveCmd.PersistentFlags().String("httpPort", "",
		"sets http listen port; will override config file settings")

	serveCmd.PersistentFlags().String("defaultURL", "",
		"sets default url; will override config file settings")

	serveCmd.PersistentFlags().String("shortURL", "",
		"sets short url; will override config file settings")

	serveCmd.PersistentFlags().String("dnsAddress", "",
		"sets dns listen address; will override config file settings")

	serveCmd.PersistentFlags().String("dnsPort", "",
		"sets dns listen port; will override config file settings")

	serveCmd.PersistentFlags().BoolVarP(&disableHTTP, "disableHTTP", "s", false,
		"disable HTTP server")

	serveCmd.PersistentFlags().BoolVarP(&disableDNS, "disableDNS", "d", false,
		"disable DNS server")

	serveCmd.Flags().SortFlags = false
	serveCmd.PersistentFlags().SortFlags = false
}

func startShortenServer(cfg *config.Config) {

	var logger *logrus.Logger
	logger = nil
	if !cfg.Server.DisableServerLogging {
		logger = logrus.New()
		level, err := logrus.ParseLevel(cfg.Server.LoggingLevel)
		if err != nil {
			internal.PrintError(err)
			os.Exit(6)
		}
		logger.SetLevel(level)
		logger.SetFormatter(&logrus.TextFormatter{
			FullTimestamp: true,
			DisableColors: false,
		})
		logger.SetOutput(os.Stdout)
	}

	dbStr := "user=" + cfg.Database.User +
		" password='" + cfg.Database.Password +
		"' host=" + cfg.Database.Host +
		" dbname=" + cfg.Database.Name +
		" sslmode=" + cfg.Database.SSLMode

	db, err := postgres.New(logger, dbStr)
	if err != nil {
		internal.PrintError(err)
		os.Exit(8)
	}

	var httpCfg *shorter.HTTPConfig
	var httpSrv *shorter.HTTPSrv
	httpCfg = nil
	httpSrv = nil
	if !disableHTTP {
		httpCfg = &shorter.HTTPConfig{
			TLD:           cfg.Server.TLD,
			Logger:        logger,
			DefaultURL:    cfg.HTTPServer.DefaultURL,
			ShortURL:      cfg.HTTPServer.ShortURL,
			DisableLogger: cfg.Server.DisableServerLogging,
			Port:          cfg.HTTPServer.Port,
			Address:       cfg.HTTPServer.Address,
			DB:            db,
		}
		httpSrv = shorter.NewHTTPServer(httpCfg)
	}

	var dnsCfg *shorter.DNSConfig
	var dnsSrv *shorter.DNSSrv
	dnsCfg = nil
	dnsSrv = nil
	if !disableDNS {
		dnsCfg = &shorter.DNSConfig{
			TLD:           cfg.Server.TLD,
			Logger:        logger,
			DisableLogger: cfg.Server.DisableServerLogging,
			Address:       cfg.DNSServer.Address,
			Port:          cfg.DNSServer.Port,
			DB:            db,
		}
		dnsSrv = shorter.NewDNSServer(dnsCfg)
	}

	var httpErr error
	httpErr = nil
	if httpSrv != nil {
		httpErr = shorter.StartHTTPServer(httpSrv)
	}

	var dnsErr error
	dnsErr = nil
	if dnsSrv != nil && httpErr == nil {
		dnsErr = shorter.StartDNSServer(dnsSrv)
	}

	if httpErr != nil || dnsErr != nil {
		ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
		defer cancel()
		if httpSrv != nil {
			go httpSrv.Shutdown(ctx)
		}
		if dnsSrv != nil {
			go dnsSrv.Shutdown(ctx)
		}
		<-ctx.Done()

		internal.PrintError(fmt.Sprintf("a service failed to start:\nhttp errors: %s\ndns errors: %s", httpErr, dnsErr))
		os.Exit(9)
	}

	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)
	<-c

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*15)
	defer cancel()
	httpErrS := make(chan error, 1)
	dnsErrS := make(chan error, 1)

	if httpSrv != nil {
		go func(e chan error) {
			err := httpSrv.Shutdown(ctx)
			if err != nil {
				e <- err
			}
			e <- nil
		}(httpErrS)
	} else {
		httpErrS <- nil
	}

	if dnsSrv != nil {
		go func(e chan error) {
			err := dnsSrv.Shutdown(ctx)
			if err != nil {
				e <- err
			}
			e <- nil
		}(dnsErrS)
	} else {
		dnsErrS <- nil
	}

	<-ctx.Done()

	h := <-httpErrS
	d := <-dnsErrS

	if h != nil || d != nil {
		internal.PrintError(fmt.Sprintf("http shutdown error: %s\ndns shutdown error: %s\n", h, d))
	}
}
