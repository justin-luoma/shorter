/*
 * Copyright (C) 2019 Justin Luoma <justin@justinluoma.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package cmd

import (
	"fmt"
	"os"

	"gitlab.com/justin-luoma/shorter/shorter/cmd/internal/config"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

var (
	BuildVersion = ""
	BuildTime    = ""
	BuildCommit  = ""

	cfgFile        string
	cfgError       bool
	cfgErrorString string
)

var rootCmd = &cobra.Command{
	Use:   "shorter",
	Short: "A simple url shortener and dns server written in go.",
	Run: func(cmd *cobra.Command, args []string) {
		if b, _ := cmd.Flags().GetBool("version"); b {
			printVersion()
			os.Exit(0)
		} else {
			_ = cmd.Help()
		}
	},
	PersistentPostRun: func(cmd *cobra.Command, args []string) {
		if cfgError {
			fmt.Print("\nErrors:\n", cfgErrorString)
		}
	},
}

func Execute() {
	initialize()
	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

func initialize() {
	cobra.OnInitialize(initCfg)

	rootCmd.PersistentFlags().StringVar(&cfgFile, "config", "",
		"config file (default: ./config.{yaml, json, toml, hcl, properties})")

	rootCmd.PersistentFlags().StringVarP(&config.EnvPrefix, "envPrefix", "e", "S",
		"Prefix to use for environment variables, eg S_LOGGING_LEVEL=trace where S is the prefix (default S)")

	rootCmd.Flags().BoolP("version", "V", false, "show version info")

	rootCmd.Flags().SortFlags = false
	rootCmd.PersistentFlags().SortFlags = false

	initServe()
	initConfig()
	initEnv()
}

func initCfg() {
	if cfgFile != "" {
		viper.SetConfigFile(cfgFile)
	} else {
		viper.AddConfigPath("./")
		viper.SetConfigName("config")
	}

	if err := viper.ReadInConfig(); err != nil {
		if err, ok := err.(viper.ConfigFileNotFoundError); ok {
			cfgError = true
			cfgErrorString = fmt.Sprintf("%s\n%s\n", err.Error(), "valid file extensions: yaml, json, toml, hcl, properties")
		} else {
			cfgError = true
			cfgErrorString = fmt.Sprintf("failed to read config file: %s\n", viper.ConfigFileUsed())
		}
	}
}

func printVersion() {
	fmt.Printf(
		"shorter, url shortner http and dns server\n"+
			"version: %s\n"+
			"built: %s\n"+
			"commit: %s\n"+
			"Copyright (C) 2019 Justin Luoma <justin@justinluoma.com>.\n"+
			"License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>\n"+
			"This is free software; you are free to change and redistribute it.\n"+
			"There is NO WARRANTY, to the extent permitted by law.\n", BuildVersion, BuildTime, BuildCommit)
}
