/*
 * Copyright (C) 2019 Justin Luoma <justin@justinluoma.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package config

import (
	"os"

	"github.com/spf13/pflag"
	"gitlab.com/justin-luoma/shorter/shorter/cmd/internal"
)

var flagConfig map[string]interface{}

func ValidateFlags(flag *pflag.Flag) {
	if flag.Changed && flag.Name != "disableHTTP" && flag.Name != "disableDNS" {
		name := flag.Name
		value := flag.Value.String()
		v, err := ValidateSetting(name, value)
		if err != nil {
			flagError(name, value)
		}
		flagConfig[name] = v
	}
}

func ParseFlags(flags *pflag.FlagSet) {
	flagConfig = make(map[string]interface{})
	flags.VisitAll(ValidateFlags)
}

func flagError(name, value string) {
	internal.PrintError("invalid flag value:", value, "; set for:", name)
	os.Exit(3)
}
