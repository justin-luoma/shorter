/*
 * Copyright (C) 2019 Justin Luoma <justin@justinluoma.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package config

import (
	"encoding/json"
	"errors"
	"fmt"
	"os"
	"strconv"

	"gitlab.com/justin-luoma/shorter/shorter/cmd/internal"

	"github.com/pelletier/go-toml"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gopkg.in/yaml.v2"
)

type ServerConfig struct {
	LoggingLevel         string `json:"loggingLevel"`
	TLD                  string `json:"tld"`
	DisableServerLogging bool   `json:"disableServerLogging"`
}

type DatabaseConfig struct {
	Type     string `json:"type"`
	Host     string `json:"host"`
	Name     string `json:"name"`
	User     string `json:"user"`
	Password string `json:"password"`
	SSLMode  string `json:"sslMode"`
}

type HTTPServerConfig struct {
	Address    string `json:"address"`
	Port       string `json:"port"`
	DefaultURL string `json:"defaultURL"`
	ShortURL   string `json:"shortURL"`
}

type DNSServerConfig struct {
	Address string `json:"address"`
	Port    string `json:"port"`
}

type Config struct {
	Server     ServerConfig     `json:"server"`
	Database   DatabaseConfig   `json:"database"`
	HTTPServer HTTPServerConfig `json:"httpServer"`
	DNSServer  DNSServerConfig  `json:"dnsServer"`
}

var loggingLevelValues = map[string]int{"trace": 0, "debug": 1, "info": 2, "warn": 3, "error": 4, "fatal": 5, "panic": 6}
var dbTypeValues = map[string]int{"postgres": 0}
var dbSSLModeValues = map[string]int{"disable": 0, "require": 1, "verify-ca": 2, "verify-full": 3}

var configEntries = map[string]interface{}{
	"loggingLevel":         loggingLevelValues,
	"tld":                  "string",
	"disableServerLogging": "bool",
	"databaseType":         dbTypeValues,
	"databaseHost":         "string",
	"databaseName":         "string",
	"databaseUser":         "string",
	"databasePassword":     "string",
	"databaseSSLMode":      dbSSLModeValues,
	"httpAddress":          "string",
	"httpPort":             "string",
	"defaultURL":           "string",
	"shortURL":             "string",
	"dnsAddress":           "string",
	"dnsPort":              "string",
}

var envConfigMap = map[string]string{
	"loggingLevel":         "LOGGING_LEVEL",
	"tld":                  "TLD",
	"disableServerLogging": "DISABLE_SERVER_LOGGING",
	"databaseType":         "DATABASE_TYPE",
	"databaseHost":         "DATABASE_HOST",
	"databaseName":         "DATABASE_NAME",
	"databaseUser":         "DATABASE_USER",
	"databasePassword":     "DATABASE_PASSWORD",
	"databaseSSLMode":      "DATABASE_SSLMODE",
	"httpAddress":          "HTTP_ADDRESS",
	"httpPort":             "HTTP_PORT",
	"defaultURL":           "DEFAULT_URL",
	"shortURL":             "SHORT_URL",
	"dnsAddress":           "DNS_ADDRESS",
	"dnsPort":              "DNS_PORT",
}

func Parse() (*Config, error) {
	var srv ServerConfig
	err := viper.UnmarshalKey("server", &srv)
	if err != nil {
		return nil, err
	}
	var db DatabaseConfig
	err = viper.UnmarshalKey("database", &db)
	if err != nil {
		return nil, err
	}
	var http HTTPServerConfig
	err = viper.UnmarshalKey("httpServer", &http)
	if err != nil {
		return nil, err
	}
	var dns DNSServerConfig
	err = viper.UnmarshalKey("dnsServer", &dns)
	if err != nil {
		return nil, err
	}
	c := Config{
		Server:     srv,
		Database:   db,
		HTTPServer: http,
		DNSServer:  dns,
	}

	return &c, nil
}

func (c *Config) WriteConfigFile(format string, path string) error {
	switch format {
	case "yaml":
		return c.generateYAML(path)
	case "json":
		return c.generateJSON(path)
	case "toml":
		return c.generateTOML(path)
	}

	return fmt.Errorf("invalid format: %s", format)
}

func (c Config) generateYAML(path string) error {
	b, err := yaml.Marshal(c)
	if err != nil {
		return err
	}

	return writeToFile(path, b)
}

func (c *Config) generateJSON(path string) error {
	b, err := json.MarshalIndent(*c, "", " ")
	if err != nil {
		return err
	}

	return writeToFile(path, b)
}

func (c *Config) generateTOML(path string) error {
	b, err := toml.Marshal(*c)
	if err != nil {
		return err
	}

	return writeToFile(path, b)
}

func writeToFile(path string, data []byte) error {
	f, err := os.OpenFile(path, os.O_WRONLY|os.O_CREATE, 0750)
	if err != nil {
		return err
	}
	defer f.Close()
	_, err = f.Write(data)
	if err != nil {
		return err
	}

	return f.Sync()
}

func ValidateSetting(name string, value string) (interface{}, error) {
	switch name {
	case "loggingLevel":
		if _, ok := loggingLevelValues[value]; !ok {
			return nil, errors.New("invalid setting")
		}
		return value, nil
	case "disableServerLogging":
		b, err := strconv.ParseBool(value)
		if err != nil {
			return nil, errors.New("invalid setting")
		}
		return b, nil
	case "databaseType":
		if _, ok := dbTypeValues[value]; !ok {
			return nil, errors.New("invalid setting")
		}
		return value, nil
	case "databaseSSLMode":
		if _, ok := dbSSLModeValues[value]; !ok {
			return nil, errors.New("invalid setting")
		}
		return value, nil
	case "httpPort":
		p, err := strconv.Atoi(value)
		if err != nil || p < 0 || p > 65536 {
			return nil, errors.New("invalid setting")
		}
		return value, nil
	case "dnsPort":
		p, err := strconv.Atoi(value)
		if err != nil || p < 0 || p > 65536 {
			return nil, errors.New("invalid setting")
		}
		return value, nil
	default:
		return value, nil
	}
	//return nil, errors.New("invalid setting")
}

func ParseAndMerge(cmd *cobra.Command) *Config {
	cfg, err := Parse()
	if err != nil {
		internal.PrintError("failed to parse config file:", err)
		os.Exit(1)
	}
	ParseENV()
	for k, v := range envConfig {
		if v != nil {
			cfg.merge(k, v)
		}
	}
	ParseFlags(cmd.PersistentFlags())
	for k, v := range flagConfig {
		cfg.merge(k, v)
	}

	return cfg
}

func (c *Config) merge(name string, value interface{}) {
	switch name {
	case "loggingLevel":
		c.Server.LoggingLevel = value.(string)
		return
	case "tld":
		c.Server.TLD = value.(string)
		return
	case "disableServerLogging":
		c.Server.DisableServerLogging = value.(bool)
		return
	case "databaseType":
		c.Database.Type = value.(string)
		return
	case "databaseHost":
		c.Database.Host = value.(string)
		return
	case "databaseName":
		c.Database.Name = value.(string)
		return
	case "databaseUser":
		c.Database.User = value.(string)
		return
	case "databasePassword":
		c.Database.Password = value.(string)
		return
	case "databaseSSLMode":
		c.Database.SSLMode = value.(string)
		return
	case "httpAddress":
		c.HTTPServer.Address = value.(string)
		return
	case "httpPort":
		c.HTTPServer.Port = value.(string)
		return
	case "defaultURL":
		c.HTTPServer.DefaultURL = value.(string)
		return
	case "shortURL":
		c.HTTPServer.ShortURL = value.(string)
		return
	case "dnsAddress":
		c.DNSServer.Address = value.(string)
		return
	case "dnsPort":
		c.DNSServer.Port = value.(string)
		return
	}
	internal.PrintError("invalid setting, name:", name)
	os.Exit(4)
}
