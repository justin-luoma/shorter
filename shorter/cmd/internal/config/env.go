/*
 * Copyright (C) 2019 Justin Luoma <justin@justinluoma.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package config

import (
	"fmt"
	"os"
	"strings"

	"github.com/spf13/viper"
	"gitlab.com/justin-luoma/shorter/shorter/cmd/internal"
)

var EnvPrefix = "S"

var parsed = false

var envConfig map[string]interface{}

func PrintENVHelp() {
	for k, v := range configEntries {
		k = envConfigMap[k]
		switch v.(type) {
		case map[string]int:
			var out strings.Builder
			for values := range v.(map[string]int) {
				out.WriteString(values)
				out.WriteString(" ")
			}
			fmt.Printf("%s_%s; valid options: %s\n", EnvPrefix, k, out.String())
		case string:
			fmt.Printf("%s_%s; type: %s\n", EnvPrefix, k, v)
		}
	}
	fmt.Printf("\nenv prefix: %s\n", EnvPrefix)
}

func ParseENV() {
	viper.SetEnvPrefix(EnvPrefix)
	viper.AutomaticEnv()
	var env = configEntries
	for k := range env {
		envK := envConfigMap[k]
		env[k] = viper.Get(envK)
		if v, ok := env[k].(string); ok {
			s, err := ValidateSetting(k, v)
			if err != nil {
				envError(k, v)
			}
			env[k] = s
		}
	}
	envConfig = env
	parsed = true
}

func PrintENVConfig() {
	if parsed {
		var out strings.Builder
		for k, v := range envConfig {
			out.WriteString(fmt.Sprintf("%s_%s=%v\n", EnvPrefix, envConfigMap[k], v))
		}
		fmt.Print(out.String())
		return
	}
	ParseENV()
	PrintENVConfig()
}

func envError(name, value string) {
	internal.PrintError("invalid env value:", value, "; set for:", name)
	os.Exit(4)
}
