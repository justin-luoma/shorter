/*
 * Copyright (C) 2019 Justin Luoma <justin@justinluoma.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package cmd

import (
	"fmt"
	"os"

	"gitlab.com/justin-luoma/shorter/shorter/cmd/cfg"
	"gitlab.com/justin-luoma/shorter/shorter/cmd/internal/config"

	"github.com/spf13/cobra"
)

var configCmd = &cobra.Command{
	Use: "config",
	Long: `shorten can be configured in several ways;
in order of precedence command line flags, environment variables, and config files`,
	Run: func(cmd *cobra.Command, args []string) {
		if v, err := cmd.Flags().GetBool("validate"); err == nil && v {
			_, err = config.Parse()
			if err != nil {
				fmt.Println("parsing error:\n", err)
				os.Exit(1)
			}
			fmt.Println("config file parsed successfully")
			os.Exit(0)
		}
		_ = cmd.Help()
	},
}

func initConfig() {
	rootCmd.AddCommand(configCmd)

	configCmd.Flags().BoolP("validate", "v", false,
		"validate current config file")

	configCmd.AddCommand(cfg.GenerateCmd)
	configCmd.AddCommand(cfg.EnvCmd)
	configCmd.AddCommand(cfg.PrintCmd)

	cfg.InitGenerate()
	config.InitDefault()
}
