/*
 * Copyright (C) 2019 Justin Luoma <justin@justinluoma.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package shorter

import (
	"context"
	"fmt"

	"github.com/miekg/dns"
	"github.com/sirupsen/logrus"
	"gitlab.com/justin-luoma/shorter/schema"
)

type DNSConfig struct {
	Address       string
	Port          string
	TLD           string
	DB            Repo
	DisableLogger bool
	Logger        *logrus.Logger
}

type DNSSrv struct {
	db            Repo
	disableLogger bool
	logger        *logrus.Logger
	srv           *dns.Server
}

func NewDNSServer(cfg *DNSConfig) *DNSSrv {

	s := DNSSrv{
		db:            cfg.DB,
		disableLogger: cfg.DisableLogger,
		logger:        cfg.Logger,
	}

	d := dns.NewServeMux()
	d.HandleFunc(cfg.TLD, s.dnsHandle)
	dnsSrv := &dns.Server{
		Addr:    cfg.Address + ":" + cfg.Port,
		Net:     "udp",
		Handler: d,
	}

	s.srv = dnsSrv

	return &s
}

func (s *DNSSrv) log(level logrus.Level, format string, args ...interface{}) {
	if !s.disableLogger {
		s.logger.Logf(level, format, args...)
	}
}

func (s *DNSSrv) dnsHandle(w dns.ResponseWriter, r *dns.Msg) {
	s.log(logrus.DebugLevel, "dns: got dns request %+v\n", r)
	m := new(dns.Msg)
	m.SetReply(r)
	m.Compress = false

	if r.Opcode == dns.OpcodeQuery {
		for _, q := range m.Question {
			if q.Qtype == dns.TypeA {
				s.log(logrus.TraceLevel, "got dns query for for %s\n", q.Name)
				record, err := s.db.GetOne(q.Name, schema.DNSRecord)
				if err != nil {
					s.log(logrus.ErrorLevel, "error getting dns record from database: %q\n", err)
				}
				if record != nil && err == nil {
					s.log(logrus.TraceLevel, "dns response to query, %s: %s\n", q.Name, record.Data)
					rr, err := dns.NewRR(fmt.Sprintf("%s A %s", q.Name, record.Data))
					if err == nil {
						m.Answer = append(m.Answer, rr)
					}
				}
			}
		}
	}

	err := w.WriteMsg(m)
	if err != nil {
		s.log(logrus.WarnLevel, "%q\n", err)
	}
}

func (s *DNSSrv) Start() error {
	s.log(logrus.InfoLevel, "start dns server on: %s\n", s.srv.Addr)
	return s.srv.ListenAndServe()
}

func (s *DNSSrv) Shutdown(ctx context.Context) error {
	s.log(logrus.InfoLevel, "shutting down dns server")
	return s.srv.ShutdownContext(ctx)
}
