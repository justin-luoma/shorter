/*
 * Copyright (C) 2019 Justin Luoma <justin@justinluoma.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package schema

import (
	"fmt"
	"strings"
)

type RecordType uint32

func (recordType RecordType) String() string {
	switch recordType {
	case DNSRecord:
		return string([]byte("dns"))
	case RedirectRecord:
		return string([]byte("redirect"))
	default:
		return "unknown"
	}
}

func ParseRecordType(rec string) (RecordType, error) {
	switch strings.ToLower(rec) {
	case "dns":
		return DNSRecord, nil
	case "redirect":
		return RedirectRecord, nil
	default:
		var r RecordType
		return r, fmt.Errorf("not a valid record type: %q", r)
	}
}

const (
	DNSRecord RecordType = iota
	RedirectRecord
)
