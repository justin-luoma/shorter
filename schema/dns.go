/*
 * Copyright (C) 2019 Justin Luoma <justin@justinluoma.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package schema

import "time"

type DNS struct {
	Name    string    `db:"name"`
	IP      string    `db:"ip"`
	Created time.Time `db:"created"`
}

func (d *DNS) RecordType() RecordType {
	return DNSRecord
}

func (d *DNS) Reference() string {
	return d.Name
}

func (d *DNS) Value() string {
	return d.IP
}

func (d *DNS) Timestamp() time.Time {
	return d.Created
}
