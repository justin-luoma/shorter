/*
 * Copyright (C) 2019 Justin Luoma <justin@justinluoma.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package schema

import "time"

type DTO struct {
	Name    string
	Data    string
	Created time.Time
	Type    RecordType
}

func (d *DTO) RecordType() RecordType {
	return d.Type
}

func (d *DTO) Reference() string {
	return d.Name
}

func (d *DTO) Value() string {
	return d.Data
}

func (d *DTO) Timestamp() time.Time {
	return d.Created
}
