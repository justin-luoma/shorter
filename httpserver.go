/*
 * Copyright (C) 2019 Justin Luoma <justin@justinluoma.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package shorter

import (
	"context"
	"net/http"
	"strings"
	"time"

	"github.com/gorilla/mux"
	"github.com/sirupsen/logrus"
	"gitlab.com/justin-luoma/shorter/schema"
)

type HTTPConfig struct {
	Address       string
	Port          string
	TLD           string
	DB            Repo
	ShortURL      string
	DefaultURL    string
	DisableLogger bool
	Logger        *logrus.Logger
}

type HTTPSrv struct {
	db            Repo
	disableLogger bool
	logger        *logrus.Logger
	srv           *http.Server
	shortURL      string
	defaultURL    string
	tld           string
}

func NewHTTPServer(cfg *HTTPConfig) *HTTPSrv {

	s := HTTPSrv{
		db:            cfg.DB,
		disableLogger: cfg.DisableLogger,
		logger:        cfg.Logger,
		shortURL:      cfg.ShortURL,
		defaultURL:    cfg.DefaultURL,
		tld:           cfg.TLD,
	}

	r := mux.NewRouter()
	r.HandleFunc("/s", s.createShortURLHandler).Queries("url", "").Queries("slug", "")
	r.HandleFunc("/n", s.createDNSNameHandler).Queries("sub", "").Queries("ip", "")
	r.HandleFunc("/{slug:[a-zA-Z0-9]+}", s.shortURLHandler)
	r.HandleFunc("/", s.catchAllHandler)
	httpSrv := &http.Server{
		Addr:         cfg.Address + ":" + cfg.Port,
		WriteTimeout: time.Second * 15,
		ReadTimeout:  time.Second * 15,
		IdleTimeout:  time.Second * 60,
		Handler:      removeTrailingSlash(r),
	}

	s.srv = httpSrv

	return &s
}

func (s *HTTPSrv) log(level logrus.Level, format string, args ...interface{}) {
	if !s.disableLogger {
		s.logger.Logf(level, format, args...)
	}
}

func (s *HTTPSrv) createShortURLHandler(w http.ResponseWriter, r *http.Request) {
	url := r.URL.Query().Get("url")
	if url == "" {
		http.Error(w, "", http.StatusBadRequest)
		return
	}

	slug := r.URL.Query().Get("slug")
	if slug == "" {
		http.Error(w, "", http.StatusBadRequest)
		return
	}

	record, err := s.db.GetOne(slug, schema.RedirectRecord)
	if err == nil {
		_, _ = w.Write([]byte(s.shortURL + "/" + slug + " : " + record.Data))
		return
	}

	err = s.db.Create(&schema.Redirect{Slug: slug, URL: url})
	if err != nil {
		s.log(logrus.ErrorLevel, "%q\n", err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusCreated)
	_, _ = w.Write([]byte(s.shortURL + "/" + slug))
}

func (s *HTTPSrv) createDNSNameHandler(w http.ResponseWriter, r *http.Request) {
	sub := r.URL.Query().Get("sub")
	if sub == "" {
		http.Error(w, "", http.StatusBadRequest)
		return
	}

	sub = strings.TrimSuffix(sub, ".")

	name := sub + "." + s.tld + "."

	record, err := s.db.GetOne(name, schema.DNSRecord)
	if err == nil {
		_, _ = w.Write([]byte(name + " " + record.Data))
		return
	}

	ip := r.URL.Query().Get("ip")
	if ip == "" {
		http.Error(w, "", http.StatusBadRequest)
		return
	}

	err = s.db.Create(&schema.DNS{Name: name, IP: ip})
	if err != nil {
		s.log(logrus.ErrorLevel, "%q\n", err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusCreated)
	_, _ = w.Write([]byte(name + " " + ip))
}

func (s *HTTPSrv) shortURLHandler(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	slug, ok := vars["slug"]

	if !ok {
		http.Error(w, "", http.StatusBadRequest)
		return
	}

	record, err := s.db.GetOne(slug, schema.RedirectRecord)
	if err != nil {
		http.NotFound(w, r)
		return
	}

	http.Redirect(w, r, record.Data, http.StatusMovedPermanently)
}

func (s *HTTPSrv) catchAllHandler(w http.ResponseWriter, r *http.Request) {
	http.Redirect(w, r, s.defaultURL, http.StatusMovedPermanently)
}

func (s *HTTPSrv) Start() error {
	s.log(logrus.InfoLevel, "starting http server on: %s\n", s.srv.Addr)
	return s.srv.ListenAndServe()
}

func (s *HTTPSrv) Shutdown(ctx context.Context) error {
	s.log(logrus.InfoLevel, "shutting down http server\n")
	return s.srv.Shutdown(ctx)
}

func removeTrailingSlash(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		r.URL.Path = strings.TrimSuffix(r.URL.Path, "/")
		next.ServeHTTP(w, r)
	})
}

