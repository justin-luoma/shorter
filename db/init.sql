CREATE DATABASE names;

\connect names;

CREATE TABLE redirect (
                          slug text PRIMARY KEY,
                          url text NOT NULL,
                          created timestamptz NOT NULL DEFAULT now()
);

CREATE TABLE dns (
                     name text PRIMARY KEY,
                     ip text NOT NULL,
                     created timestamptz NOT NULL DEFAULT now()
);

-- Replace 'go.' with your short domain and replace '10.0.0.1' with the ip address of your shorter servers
-- INSERT INTO dns (name, ip) VALUES ('go.', '10.0.0.1');
