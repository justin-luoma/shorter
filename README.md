# shorter

shorter is a simple url shortener web and dns server

## What is this?

shorter is meant to be used inside your network to shorten the urls you need to type

example usage:

setup your dns server (whether it be your router or an actual dns server) to forward dns requests for the go tld: *.go to shorter server

then if you type ```go``` (http://go.) into your browser and you'll be redirected to whatever you set ```defaultURL```
 to in your config 
 
 or if you created the gitlab slug seen below http://go/gitlab will redirect to https://gitlab.com
 
 ## Database
 
 postgres is currently the only supported database, there's an example init file in [db](https://gitlab.com/justin-luoma/shorter/blob/master/db/init.sql)
 
## Installation

### Download from [Releases](https://gitlab.com/justin-luoma/shorter/-/releases) page

or

```shell
go install gitlab.com/justin-luoma/shorter/shorter
```

## Usage

to generate an example config.yaml file in current directory

```shell
$GOPATH/bin/shorter config generate
```
to start the server using the config file in the current directory

```shell
$GOPATH/bin/shorter serve
```

for help configuring shorten server

```shell
$GOPATH/bin/shorter config
```

create a new short url:

```shell
curl host:port/s?slug=gitlab&url=https://gitlab.com
```

create a new dns entry:

```shell
curl host:port/n?sub=dns&ip=192.168.0.1
```

## License
[GPLv3+](https://gnu.org/licenses/gpl.html)

## Todo

* Add tests
* Documentation
* add more database(repo) support
* https/easier custom http server configuration
* tcp dns server
* management page
