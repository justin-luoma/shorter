/*
 * Copyright (C) 2019 Justin Luoma <justin@justinluoma.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package postgres

import (
	"errors"
	"fmt"

	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
	"github.com/sirupsen/logrus"
	"gitlab.com/justin-luoma/shorter/schema"
)

type Repo struct {
	logger *logrus.Logger
	db     *sqlx.DB
}

func New(logger *logrus.Logger, connStr string) (*Repo, error) {
	db, err := sqlx.Connect("postgres", connStr)
	if err != nil {
		return nil, err
	}
	pg := Repo{
		logger: logger,
		db:     db,
	}
	return &pg, nil
}

func (pg *Repo) Name() string {
	return "postgres"
}

func (pg *Repo) GetAll() (*[]schema.DTO, error) {
	var dns []schema.DNS
	err := pg.db.Select(&dns, "SELECT * from dns")
	if err != nil {
		return nil, err
	}
	var redirect []schema.Redirect
	err = pg.db.Select(&redirect, "SELECT * FROM redirect")
	if err != nil {
		return nil, err
	}

	dnsDTO := convert(&dns)
	redDTO := convert(&redirect)
	dto := append(dnsDTO.([]schema.DTO), redDTO.([]schema.DTO)...)

	return &dto, nil
}

func (pg *Repo) GetOne(ref string, rType schema.RecordType) (*schema.DTO, error) {
	var rt schema.DTO
	switch rType {
	case schema.DNSRecord:
		var dns schema.DNS
		err := pg.db.Get(&dns, "SELECT * FROM dns WHERE name = $1", ref)
		if err != nil {
			return nil, err
		}
		tmp := convert(&dns)
		rt = tmp.(schema.DTO)
	case schema.RedirectRecord:
		var redirect schema.Redirect
		err := pg.db.Get(&redirect, "SELECT * FROM redirect WHERE slug = $1", ref)
		if err != nil {
			return nil, err
		}
		tmp := convert(&redirect)
		rt = tmp.(schema.DTO)
	}
	return &rt, nil
}

func (pg *Repo) Update(ref string, a schema.Abstract) error {
	switch a.RecordType() {
	case schema.DNSRecord:
		tx, err := pg.db.Beginx()
		if err != nil {
			return err
		}
		_, err = tx.Exec("UPDATE dns SET name = $1, ip = $2 WHERE name = $3", a.Reference(), a.Value(), ref)
		if err != nil {
			txErr := tx.Rollback()
			return fmt.Errorf("error during transaction exec: %q, errors during rollback: %q", err, txErr)
		}
		err = tx.Commit()
		if err != nil {
			return err
		}
		return nil
	case schema.RedirectRecord:
		tx, err := pg.db.Beginx()
		if err != nil {
			return err
		}
		_, err = tx.Exec("UPDATE redirect SET slug = $1, url = $2 WHERE slug = $3", a.Reference(), a.Value(), ref)
		if err != nil {
			txErr := tx.Rollback()
			return fmt.Errorf("error during transaction exec: %q, errors during rollback: %q", err, txErr)
		}
		err = tx.Commit()
		if err != nil {
			return err
		}
		return nil
	}
	return errors.New("unknown type")
}

func (pg *Repo) Create(a schema.Abstract) error {
	switch a.RecordType() {
	case schema.DNSRecord:
		tx, err := pg.db.Beginx()
		if err != nil {
			return err
		}
		_, err = tx.Exec("INSERT INTO dns (name, ip) VALUES ($1, $2)", a.Reference(), a.Value())
		if err != nil {
			txErr := tx.Rollback()
			return fmt.Errorf("error during transaction exec: %q, errors during rollback: %q", err, txErr)
		}
		err = tx.Commit()
		if err != nil {
			return err
		}
		return nil
	case schema.RedirectRecord:
		tx, err := pg.db.Beginx()
		if err != nil {
			return err
		}
		_, err = tx.Exec("INSERT INTO redirect (slug, url) VALUES ($1, $2)", a.Reference(), a.Value())
		if err != nil {
			txErr := tx.Rollback()
			return fmt.Errorf("error during transaction exec: %q, errors during rollback: %q", err, txErr)
		}
		err = tx.Commit()
		if err != nil {
			return err
		}
		return nil
	}
	return errors.New("unknown type")
}

func (pg *Repo) Delete(ref string, rType schema.RecordType) error {
	switch rType {
	case schema.DNSRecord:
		tx, err := pg.db.Beginx()
		if err != nil {
			return err
		}
		_, err = tx.Exec("DELETE FROM dns WHERE name = $1", ref)
		if err != nil {
			txErr := tx.Rollback()
			return fmt.Errorf("error during transaction exec: %q, errors during rollback: %q", err, txErr)
		}
		err = tx.Commit()
		if err != nil {
			return err
		}
		return nil
	case schema.RedirectRecord:
		tx, err := pg.db.Beginx()
		if err != nil {
			return err
		}
		_, err = tx.Exec("DELETE FROM redirect WHERE slug = $1", ref)
		if err != nil {
			txErr := tx.Rollback()
			return fmt.Errorf("error during transaction exec: %q, errors during rollback: %q", err, txErr)
		}
		err = tx.Commit()
		if err != nil {
			return err
		}
		return nil
	}
	return errors.New("unknown type")
}

func (pg *Repo) Close() error {
	return pg.db.Close()
}

func convert(d interface{}) interface{} {
	switch d := d.(type) {
	case *schema.DNS:
		dto := schema.DTO{
			Name:    d.Name,
			Data:    d.IP,
			Created: d.Created,
			Type:    schema.DNSRecord,
		}
		return dto
	case *schema.Redirect:
		dto := schema.DTO{
			Name:    d.Slug,
			Data:    d.URL,
			Created: d.Created,
			Type:    schema.RedirectRecord,
		}
		return dto
	case *[]schema.DNS:
		dto := make([]schema.DTO, len(*d))
		for k, v := range *d {
			dto[k].Name = v.Reference()
			dto[k].Data = v.Value()
			dto[k].Created = v.Timestamp()
			dto[k].Type = schema.DNSRecord
		}
		return dto
	case *[]schema.Redirect:
		dto := make([]schema.DTO, len(*d))
		for k, v := range *d {
			dto[k].Name = v.Reference()
			dto[k].Data = v.Value()
			dto[k].Created = v.Timestamp()
			dto[k].Type = schema.RedirectRecord
		}
		return dto
	}
	return nil
}
