/*
 * Copyright (C) 2019 Justin Luoma <justin@justinluoma.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package shorter

import (
	"context"
	"fmt"
	"os"
	"os/signal"
	"time"

	"github.com/sirupsen/logrus"
)

type Config struct {
	Logger               *logrus.Logger
	DB                   Repo
	TLD                  string
	DefaultURL           string
	ShortURL             string
	DisableServerLogging bool
	DNSPort              string
	DNSAddress           string
	HTTPPort             string
	HTTPAddress          string
}

func StartWithDefaults(repo Repo, tld string, defaultURL string, shortURL string) error {
	logger := logrus.New()
	logger.SetLevel(logrus.TraceLevel)
	logger.SetFormatter(&logrus.TextFormatter{
		FullTimestamp: true,
		DisableColors: false,
	})
	logger.SetReportCaller(true)
	logger.SetOutput(os.Stdout)

	httpCfg := HTTPConfig{
		TLD:           tld,
		Logger:        logger,
		DefaultURL:    defaultURL,
		ShortURL:      shortURL,
		DisableLogger: false,
		DB:            repo,
		Port:          "8080",
		Address:       "0.0.0.0",
	}

	dnsCfg := DNSConfig{
		Address:       "0.0.0.0",
		Port:          "5353",
		DB:            repo,
		DisableLogger: false,
		Logger:        logger,
		TLD:           tld,
	}

	httpSrv := NewHTTPServer(&httpCfg)
	dnsSrv := NewDNSServer(&dnsCfg)

	err := startServices(httpSrv, dnsSrv)
	if err != nil {
		return err
	}

	waitForInterrupt()

	err = shutdownServices(httpSrv, dnsSrv)
	if err != nil {
		return err
	}

	return nil
}

func StartWithConfig(config *Config) error {
	httpCfg := HTTPConfig{
		TLD:           config.TLD,
		Logger:        config.Logger,
		DefaultURL:    config.DefaultURL,
		ShortURL:      config.ShortURL,
		DisableLogger: config.DisableServerLogging,
		DB:            config.DB,
		Port:          config.HTTPPort,
		Address:       config.HTTPAddress,
	}

	dnsCfg := DNSConfig{
		Address:       config.DNSAddress,
		Port:          config.DNSPort,
		DB:            config.DB,
		DisableLogger: config.DisableServerLogging,
		Logger:        config.Logger,
		TLD:           config.TLD,
	}

	httpSrv := NewHTTPServer(&httpCfg)
	dnsSrv := NewDNSServer(&dnsCfg)

	err := startServices(httpSrv, dnsSrv)
	if err != nil {
		return err
	}

	waitForInterrupt()

	err = shutdownServices(httpSrv, dnsSrv)
	if err != nil {
		return err
	}

	return nil
}

func StartHTTPServer(srv *HTTPSrv) error {
	err := make(chan error, 1)
	go func() {
		httpErr := srv.Start()
		if httpErr != nil {
			err <- httpErr
		}
	}()

	select {
	case e := <-err:
		return e
	case <-time.After(time.Second * 5):
		return nil
	}
}

func StartDNSServer(srv *DNSSrv) error {
	err := make(chan error, 1)
	go func() {
		dnsErr := srv.Start()
		if err != nil {
			err <- dnsErr
		}
	}()

	select {
	case e := <-err:
		return e
	case <-time.After(time.Second * 5):
		return nil
	}
}

func startServices(httpSrv *HTTPSrv, dnsSrv *DNSSrv) error {
	httpErr := StartHTTPServer(httpSrv)

	var dnsErr error
	if httpErr == nil {
		dnsErr = StartDNSServer(dnsSrv)
	}

	if httpErr != nil || dnsErr != nil {
		ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
		defer cancel()
		go httpSrv.Shutdown(ctx)
		go dnsSrv.Shutdown(ctx)
		<-ctx.Done()
		return fmt.Errorf("a service failed to start:\nhttp errors: %s\ndns errors: %s", httpErr, dnsErr)
	}

	return nil
}

func waitForInterrupt() {
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)
	<-c
}

func shutdownServices(httpSrv *HTTPSrv, dnsSrv *DNSSrv) error {
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*15)
	defer cancel()
	httpErr := make(chan error, 1)
	dnsErr := make(chan error, 1)

	go func(e chan error) {
		err := httpSrv.Shutdown(ctx)
		if err != nil {
			e <- err
		}
		e <- nil
	}(httpErr)

	go func(e chan error) {
		err := dnsSrv.Shutdown(ctx)
		if err != nil {
			e <- err
		}
		e <- nil
	}(dnsErr)

	<-ctx.Done()

	h := <-httpErr
	d := <-dnsErr

	if h != nil || d != nil {
		return fmt.Errorf("http shutdown error: %s\ndns shutdown error: %s", h, d)
	}

	return nil
}
