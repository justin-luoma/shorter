/*
 * Copyright (C) 2019 Justin Luoma <justin@justinluoma.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package shorter

import (
	"fmt"
	"strings"

	"github.com/sirupsen/logrus"
	"gitlab.com/justin-luoma/shorter/repos/postgres"
	"gitlab.com/justin-luoma/shorter/schema"
)

type Repo interface {
	GetAll() (*[]schema.DTO, error)
	GetOne(ref string, rType schema.RecordType) (*schema.DTO, error)
	Update(ref string, a schema.Abstract) error
	Create(a schema.Abstract) error
	Delete(ref string, rType schema.RecordType) error
	Close() error
}

func NewRepo(repoType string, logger *logrus.Logger, connStr string) (Repo, error) {
	switch strings.ToLower(repoType) {
	case "postgres":
		return postgres.New(logger, connStr)
	}

	return nil, fmt.Errorf("repo type: %q not supported", repoType)
}
